-- DB作成
create database usermanagement DEFAULT CHARACTER SET utf8;
-- データベース使用状態
use usermanagement;

-- 商品マスタ
create table user(
    id          SERIAL         NOT NULL  UNIQUE PRIMARY KEY AUTO_INCREMENT COMMENT 'id',
    login_id    varchar (255)  NOT NULL  UNIQUE                        COMMENT 'お名前',
    name        varchar (255)  NOT NULL                              COMMENT '性別',
    birth_date  DATE           NOT NULL                              COMMENT '年齢',
    password    varchar (255)  NOT NULL                              COMMENT 'パスワード',
    create_date DATETIME       NOT NULL                              COMMENT '作成日時',
    update_date DATETIME       NOT NULL                              COMMENT '更新日時'
    );

--テーブルを入れる
INSERT INTO user(id, login_id, name, birth_date, password, create_date, update_date )
values(1,'admin', '管理者','2019-09-07','password', NOW(),NOW());
INSERT INTO user(id, login_id, name, birth_date, password, create_date, update_date )
values(2,'a', '者','2019-09-07','p', NOW(),NOW());
INSERT INTO user(id, login_id, name, birth_date, password, create_date, update_date )
values(3,'three', '','2020-01-07','p', NOW(),NOW());

INSERT INTO user(id, login_id, name, birth_date, password, create_date, update_date )
values(4,'fox', '者','2019-12-07','p', NOW(),NOW());


select * from user WHERE id NOT IN (1);

SELECT login_id, name,birth_date FROM user;

SELECT login_id, name,birth_date FROM user WHERE login_id = 'a'and password ='p'AND birth_date BETWEEN '2018-01-01' AND '2020-03-01';

select * from user WHERE id=2;

UPDATE user SET password ='a',name='山田',create_date=now(),update_date=now() WHERE id = 3;

UPDATE user SET password ='a',name='ゴンザレスさん',birth_date='2020-12-07' WHERE id = 3;
DELETE FROM user WHERE id = 7;
DELETE FROM user WHERE id = 3;
DELETE FROM user WHERE id = 4;

UPDATE user SET  password= 'y', name = 'y',birth_date='2020-12-07'  WHERE id = 16;
UPDATE user SET  name = 'a',birth_date='2020-12-07'  WHERE id = 16;


SELECT login_id, name,birth_date FROM user WHERE login_id = 'a'and password ='p'AND birth_date BETWEEN '2018-01-01' AND '2020-03-01';

SELECT name FROM user WHERE name LIKE "%山%";



SELECT login_id, name,birth_date FROM user WHERE login_id = 'huji';

SELECT login_id, name,birth_date FROM user WHERE name LIKE '%a%';

SELECT login_id, name,birth_date FROM user WHERE 
birth_date BETWEEN '2018-01-01' AND '2020-03-01';
