<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<title>ユーザー一覧</title>
	<link rel="stylesheet" href="css/NewFile.css">
</head>
<body>
    <div class="container" id="container">
		<form class=inputform action="UserListServlet" method="post">
			<div class="username">
				<p>${userInfo.name} さん </p>
				<a href="LogoutServlet">ログアウト</a>
				<br>
				<a href="UserNewServlet">新規登録</a>
			</div>
			<h2>ユーザー一覧</h2>
			        <div class="form-group">
			          <label for="exampleInputEmail1">ログインID</label>
			          <input type="text" name="loginid" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
			        </div>
			        <div class="form-group">
			          <label for="exampleInputPassword1">ユーザー名</label>
			          <input type="text"name="name" class="form-control" id="exampleInputPassword1">
			   		 </div>
		   			 <div class="">
				        <label>生年月日</label><br>
				          <div class="date m-5">
				              <input class="col-sm-5" name="birthstart" type="date" name="birthstart" value="" class="date_input">
				              <p class="col-sm-1"> ~ </p>
			    	          <input class="col-sm-5" name="date-end" type="date" name="birthdayend" value="" class="date_input">
            			  </div>
					  </div>
			<button type="submit" class="btn btn-primary">検索</button>
        </form>

        <table class="table table-striped mg-30px">
          <thead>
            <tr>
              <th scope="col">ログインID</th>
              <th scope="col">ユーザー名</th>
              <th scope="col">生年月日</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <c:forEach var="user" items="${list}" >
	          <tbody>
	            <tr>
	              <th scope="row">${user.loginId}</th>
	              <td>${user.name}</td>
	              <td>${user.birthDate}</td>
	              <td>
	              <!-- TODO 未実装；ログインボタンの表示制御を行う -->
	                <button type="button" class="btn btn-outline-success">
	                	<a class="btn btn-primary" href="UserDetailServle?id=${user.id}">詳細</a>
	                </button>
           			<c:if test="${userInfo.id==user.id || userInfo.id==1}">
		                <button type="button" class="btn btn-outline-danger">
		                	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
		                </button>
	                </c:if>
	                <c:if test="${userInfo.id==1}">
		                <button type="button" class="btn btn-outline-warning">
	                	<a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
	                </button>
					</c:if>
	              </td>
	            </tr>
	          </tbody>
          </c:forEach>
        </table>
	</div>
</body>
</html>