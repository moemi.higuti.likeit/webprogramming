<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<title>ユーザー情報更新</title>
	<link rel="stylesheet" href="css/style_input.css">
</head>
<body>
    <div class="container" id="container">
		<form class=inputform  action="UserUpdateServlet"  method="post">
			<div class="username">
				<p>${userInfo.name} さん</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
			<h2>ユーザー情報更新</h2>
			<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
			<input type="hidden" value="${user.id}" name="id">
        <div class="form-group">
          <label for="exampleInputEmail1">ログインID</label>
          <p class="cstinfo border-bottom">${user.loginId} </p>
        </div>
        <div class="form-group">
          <label for="exampleInputPassword1">パスワード</label>
          <input id="password" name="password1" type="password" class="form-control" id="exampleInputPassword1">
		</div>
		<div class="form-group">
          <label for="exampleInputPassword1">パスワード（確認用）</label>
          <input id="password_con" name="password2" type="password" class="form-control" id="exampleInputPassword1">
		</div>
        <div class="form-group">
          <label for="inputAddress">ユーザー名</label>
          <input name="name" type="text" class="form-control" value="${user.name}" id="inputAddress">
        </div>
        <div class="form-group">
            <label for="inputAddress">生年月日</label>
            <input name="birth_date" type="text" class="form-control"value="${user.birthDate}" id="inputAddress">
          </div>
			<button onclick="recheck()"  id="check" type="submit" class="btn btn-primary">更新</button>
			<a class="back" href="UserListServlet">戻る</a>
		</form>
	</div>

	<script type="text/javascript">

		console.log("");
		function recheck(){
			var pas = document.getElementByid=("password");
			var pas_con = document.getElementByid=("password_con");
			var submit =true;

			if (pas.value != pas_con.value){
				alert("パスワードが違います");
				submit = false;
			}

		}

	</script>
</body>
</html>