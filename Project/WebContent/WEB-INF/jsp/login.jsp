<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<title>ログイン画面</title>
	<link rel="stylesheet" href="css/style.css">
</head>
<body>

    <div class="container" id="container" >
	<h2>ログイン画面</h2>
	<c:if test="${errMsg != null}" >
	    <div class="alert alert-danger" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<form class="col-auto" action="LoginServlet" method="post">
		<div class="form-group">
			  <label for="exampleInputEmail1">ログインID</label>
			  <input  name="loginId" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
			  </div>
			<div class="form-group">
			  <label for="exampleInputPassword1">Password</label>
			  <input name="password" type="password" class="form-control" id="exampleInputPassword1">
			</div>

		<button type="submit" class="btn btn-primary">ログイン</button>
	</form>
	</div>
</body>
</html>