<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<title>ユーザー情報詳細参照</title>
	<link rel="stylesheet" href="css/style_input.css">
</head>
<body>
    <div class="container" id="container">

		<form class=inputform  method="post" action="UserDeleteServlet">
			<div class="username">
				<p>${userInfo.loginId} さん </p>
				<!-- ここでhiddenを-->
				<input type="hidden" value="${user.id}" name="id">

				<a href="LogoutServlet">ログアウト</a>
			</div>
            <h2>ユーザー情報詳細参照</h2>
            <div class="m-5">
                <p>ログインID：${user.name} </p>
                <p class="cstinfo border-bottom"></p>
                <P>を本当に削除してよろしいでしょうか？</P>
            </div>
            <div class="col-5 mx-auto">
		    	<div class="btn">
				    <a class="btn mr-2 btn-danger" href="UserListServlet">キャンセル</a>
				    <button type="submit" class="btn ml-2 px-4 btn-success">実行</button>
			  	</div>
	  		</div>
		</form>
	</div>
</body>
</html>