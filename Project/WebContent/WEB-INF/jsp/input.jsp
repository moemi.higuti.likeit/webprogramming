<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<title>新規登録</title>
	<link rel="stylesheet" href="css/style_input.css">
</head>
<body>
    <div class="container" id="container">

		<form class=inputform action="UserNewServlet" method="post">
			<div class="username">
				<p>${userInfo.name} さん</p>
				<a href="#">ログアウト</a>
			</div>
			<h2>ユーザー新規登録</h2>
			<div class="form-group">
			<c:if test="${errMsg != null}" >
			    <div class="alert alert-danger" role="alert">
				  ${errMsg}
				</div>
			</c:if>
				<label for="exampleInputEmail1">ログインID</label>
				<input type="text"name="loginid" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
				<small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label>
				<input type="password"name="password1" class="form-control" id="exampleInputPassword1">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード（確認）</label>
				<input type="password"name="password2" class="form-control" id="exampleInputPassword1">
			</div>
				<div class="form-group">
				<label for="inputAddress">ユーザー名</label>
				<input type="text" name="name" class="form-control" id="inputAddress">
			</div>
				<label>生年月日</label><br>
				<input type="date" name="birthDate" value="">
			<button type="submit" class="btn btn-primary">登録</button>
			<a class="back" href="UserListServlet">戻る</a>
		</form>
	</div>
</body>
</html>