<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<title>ユーザー情報詳細参照</title>
	<link rel="stylesheet" href="css/style_input.css">
</head>
<body>
    <div class="container" id="container">

		<form class=inputform>
			<div class="username">
				<p>${userInfo.name}</p>
				<a href="LogoutServlet">ログアウト</a>
			</div>
			<h2>ユーザー情報詳細参照</h2>
        <div class="mb-3 form-group">
          <label for="exampleInputEmail1">ログインID</label>
          <p class="cstinfo border-bottom">${user.loginId}</p>
        </div>
        <div class="mb-3 form-group">
          <label for="exampleInputPassword1">ユーザー名</label>
          <p class="cstinfo border-bottom">${user.name}</p>
		</div>
        <div class="mb-3 form-group">
          <label for="inputAddress">生年月日</label>
          <p class="cstinfo border-bottom">${user.birthDate}</p>
        </div>
        <div class="mb-3 form-group">
            <label for="inputAddress">登録日時</label>
            <p class="cstinfo border-bottom">${user.createDate}</p>
          </div>
          <div class="mb-3 form-group">
            <label for="inputAddress">更新日時</label>
            <p class="cstinfo border-bottom">${user.updateDate}</p>
          </div>
          <a class="back" href="UserListServlet">戻る</a>
		</form>
	</div>
</body>
</html>