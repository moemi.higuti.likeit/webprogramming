package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserNewServlet
 */
@WebServlet("/UserNewServlet")
public class UserNewServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserNewServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		HttpSession session = request.getSession();

		//さっき受け取ったセッションをに入れ変数にいれる
		User personal = (User) session.getAttribute("userInfo");

		// ユーザ一覧のサーブレットにリダイレクト
		//上で取得した情報をもってUSERLISTに行く
		//セッションしていたら
		if(personal == null) {
			response.sendRedirect("UserListServlet");
		return;
		}

		// jspの画面を出す
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("deprecation")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		// TODO  未実装：検索処理全般

		HttpSession session = request.getSession();

		//さっき受け取ったセッションをに入れ変数にいれる
		User personal = (User) session.getAttribute("userInfo");


				 // リクエストパラメータの文字コードを指定
		        request.setCharacterEncoding("UTF-8");

				// リクエストパラメータの入力項目を取得
				String login_id = request.getParameter("loginid");
				String password1 = request.getParameter("password1");
				String password2 = request.getParameter("password2");
				String name = request.getParameter("name");
				String birthdate = request.getParameter("birthDate");
				//キャストしないといけないWAKEDENAI
				//SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				//Date submitDate = (Date) sdf.parse("birthdate");
				//setSubmitDate(submitDate);

//				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//
//				Date birthdatecast = (Date) dateFormat.parse("birthdate");

				//Date birthdate = Date.parseDate(request.getParameter("birthdate"));

				// ユーザ一覧情報を取得　配列のインスタンスにはさっきの情報が入っている

//
//				String source1 = "password1";
//				String source2 = "password2";
//
//				Charset charset = StandardCharsets.UTF_8;
//
//				String algorithm = "MD5";
//
//
//				//ハッシュ生成処理
//				byte[] bytes1 = MessageDigest.getInstance(algorithm).digest(source1.getBytes(charset));
//				String result = DatatypeConverter.printHexBinary(bytes);
//				//標準出力
//				System.out.println(result);
//
//				//ハッシュ生成処理
//				byte[] bytes2 = MessageDigest.getInstance(algorithm).digest(source1.getBytes(charset));
//				String result = DatatypeConverter.printHexBinary(bytes);
//				//標準出力
//				System.out.println(result);


				if(login_id=="" || password1=="" || password2=="" || name == "" || birthdate =="") {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "項目欄が足りません");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
					dispatcher.forward(request, response);
				}

				if(!password1.equals(password2)) {
					request.setAttribute("errMsg", "パスワードが違います");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
					dispatcher.forward(request, response);
					}

				UserDao userDao = new UserDao();

				//idを探してあるかどうか調べる

				User user = userDao.findByLoginId(login_id);

				if (user != null) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "すでに登録されています");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
					dispatcher.forward(request, response);
				}

				String result = userDao.code(password1);

				userDao.Insert(login_id, result,password2,name,birthdate);




				// jspの画面を出すのではなくリダイレクトする
				//なぜなら登録情報が出てこないから
//				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
//				dispatcher.forward(request, response);

				response.sendRedirect("UserListServlet");

	}

}

/*
 * if(password1.equals(password2)) {
				request.setAttribute("errMsg", "パスワードが違います");

				// ログインjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/login.jsp");
				dispatcher.forward(request, response);
				return;
				}
 *
 *
 *if (userDao != null) {
					// リクエストスコープにエラーメッセージをセット
					request.setAttribute("errMsg", "すでに登録されています");

					// ログインjspにフォワード
					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
					dispatcher.forward(request, response);
				}
 * */
