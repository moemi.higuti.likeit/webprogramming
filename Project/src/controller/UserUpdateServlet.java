package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();

		User personal = (User) session.getAttribute("userInfo");

		// ユーザ一覧のサーブレットにリダイレクト
		//上で取得した情報をもってUSERLISTに行く
		if(personal == null) {
			response.sendRedirect("LoginServlet");
		return;
		}


		// URLからGETパラメータとしてIDを受け取る
		String id = request.getParameter("id");

		// 確認用：idをコンソールに出力
		System.out.println(id);

		// TODO  未実装：idを引数にして、idに紐づくユーザ情報を出力する

		UserDao userDao = new UserDao();
		User user = userDao.Detail(id);

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("user", user);


		// jspの画面を出す
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");


		// リクエストパラメータの入力項目を取得
		String password1 = request.getParameter("password1");
		String password2 = request.getParameter("password2");
		String name = request.getParameter("name");
		String birth_date = request.getParameter("birth_date");
		String Id = request.getParameter("id");

		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行

		if(name.equals ("") || birth_date.equals("")) {


			UserDao userDao = new UserDao();
			User user = userDao.Detail(Id);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("user", user);

			request.setAttribute("errMsg", "項目欄が足りません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;
		}



		if(!password1.equals(password2)) {

			UserDao userDao = new UserDao();
			User user = userDao.Detail(Id);

			// リクエストスコープにユーザ一覧情報をセット
			request.setAttribute("user", user);

			request.setAttribute("errMsg", "パスワードが違います");

			// ログインjspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/update.jsp");
			dispatcher.forward(request, response);
			return;

		}


		if(password1.equals ("")||password2.equals ("")) {
			UserDao userDao1 = new UserDao();
			userDao1.UpDate(name,birth_date,Id);
		}else {
			UserDao userDaoPassword = new UserDao();

			UserDao userDao = new UserDao();
			String result = userDao.code(password1);

			userDaoPassword.UpDatePassword(result,password2,name,birth_date,Id);
		}

		response.sendRedirect("UserListServlet");

	}

}
