package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserListServlet
 */
@WebServlet("/UserListServlet")
public class UserListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserListServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();

		User personal = (User) session.getAttribute("userInfo");

		// ユーザ一覧のサーブレットにリダイレクト
		//上で取得した情報をもってUSERLISTに行く
		if(personal == null) {
			response.sendRedirect("LoginServlet");
		}

		// ユーザ一覧情報を取得　配列のインスタンスにはさっきの情報が入っている
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// jspの画面を出す
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	// TODO  未実装：検索処理全般

		 // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String login_id = request.getParameter("loginid");
		String name = request.getParameter("name");
		String date_start = request.getParameter("birthstart");
		String date_end = request.getParameter("date-end");
		// ユーザ一覧情報を取得　配列のインスタンスにはさっきの情報が入っている


				UserDao userDao1 = new UserDao();
				List<User> userListByLoginId = userDao1.findSearch(login_id, name, date_start, date_end);

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userListByLoginId);

				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
				dispatcher.forward(request, response);



	}
}



/*
 * // TODO 未実装：ログインセッションがない場合、ログイン画面にリダイレクトさせる

		//セッションでその情報を保持しているものがあるか調べる
		HttpSession session = request.getSession();

		//さっき受け取ったセッションをに入れ変数にいれる
		//もともとがオブジェクト型なのでキャストする
		User personal = (User) session.getAttribute("userInfo");

		// ユーザ一覧のサーブレットにリダイレクト
		//上で取得した情報をもってUSERLISTに行く
		if(personal == null) {
		response.sendRedirect("LoginServlet");
		}

		//htmlでつっくった画面を出す
		// フォワード　サーブレットからJSPに行く
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
		dispatcher.forward(request, response);
 *
 *
 *
				UserDao userDao = new UserDao();
				List<User> userList = userDao.findPickup(login_id, name, date_start, date_end);

// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userListByBirth);


				UserDao userDao111 = new UserDao();
				List<User> userListByBirth = userDao111.findPickupByBirth(date_start, date_end);

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userListByBirth);



//名前で
				UserDao userDao11 = new UserDao();
				List<User> userListByName = userDao11.findPickupByName(name);

				// リクエストスコープにユーザ一覧情報をセット
				request.setAttribute("userList", userListByName);

				RequestDispatcher dispatcher1 = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
				dispatcher1.forward(request, response);


				//誕生日で
				UserDao userDao111 = new UserDao();
				List<User> userListByBirth = userDao111.findPickupByBirth(date_start, date_end);

				request.setAttribute("userList", userListByBirth);

				RequestDispatcher dispatcher11 = request.getRequestDispatcher("/WEB-INF/jsp/view.jsp");
				dispatcher11.forward(request, response);

 *

 *
 * */

