package dao;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

	private User User;

	/**
	 * ログインIDとパスワードに紐づくユーザ情報を返す
	 * @param loginId
	 * @param password
	 * @return
	 */
	public User findByLoginInfo(String loginId, String password) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			pStmt.setString(2, password);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			int id = rs.getInt("id");
			String loginId1 = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password1 = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");
			return new User(id, loginId1, name, birthDate, password1, createDate, updateDate);


		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	//Idがあるかどうか調べるメソッド
	public User findByLoginId(String loginId) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user WHERE login_id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, loginId);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			String loginIdData = rs.getString("login_id");
			String nameData = rs.getString("name");
			return new User(loginIdData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "select * from user WHERE id NOT IN (1);";

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	/**
	 * 全てのユーザ情報を取得する
	 * @return
	 */
	public List<User> findSearch(String login_id,String name, String date_start,String date_end) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "select * from user WHERE id NOT IN (1)";

			if(!login_id.equals("")) {
				sql += " and login_id = '" + login_id + "'";
			}
			if(!name.equals("")) {
				sql += " and name like  '%" + name + "%'";
			}
			if(!date_start.equals("")) {
				sql += " and birth_date BETWEEN '" + date_start + "'";
			}
			if(!date_end.equals("")) {
				sql += "  and '" + date_end + "'";
			}

			// SELECTを実行し、結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			// 結果表に格納されたレコードの内容を
			// Userインスタンスに設定し、ArrayListインスタンスに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				User user = new User(id, loginId, name1, birthDate, password, createDate, updateDate);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}


	public User Update(String password1, String password2, String name, Date birth_date, String Id) {
		Connection conn = null;

		code(password1);

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "UPDATE user SET password =?,name=?,birth_date=? WHERE id =?";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, password1);
			pStmt.setString(2, password2);
			pStmt.setString(3, name);
			pStmt.setDate(4, birth_date);
			pStmt.setString(5, Id);
			ResultSet rs = pStmt.executeQuery();

			// 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
			if (!rs.next()) {
				return null;
			}

			// 必要なデータのみインスタンスのフィールドに追加
			while (rs.next()) {
				int id = rs.getInt("id");
				String loginId = rs.getString("login_id");
				String name1 = rs.getString("name");
				Date birthDate = rs.getDate("birth_date");
				String password = rs.getString("password");
				String createDate = rs.getString("create_date");
				String updateDate = rs.getString("update_date");
				return new User(id, loginId, name1, birthDate, password, createDate, updateDate);

			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return User;
	}

	//新規登録画面
	public void Insert(String login_id, String password1, String password2, String name, String birthdate) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date ) values(?,?,?,?, NOW(),NOW())";

			// SQLの?パラメータに値を設定
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, login_id);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			pStmt.setString(4, password1);

			int rs = pStmt.executeUpdate();

			//		        if(rs == 0) {
			//		        	// リクエストスコープにエラーメッセージをセット
			//					request.setAttribute("errMsg", "すでに登録されています");
			//
			//					// ログインjspにフォワード
			//					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/input.jsp");
			//					dispatcher.forward(request, response);
			//		        }

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public void Delete(String id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "DELETE FROM user WHERE id = ?";

			// SQLの?パラメータに値を設定
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, id);

			int rs = pStmt.executeUpdate();


		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public User Detail(String id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			// TODO: 未実装：管理者以外を取得するようSQLを変更する
			String sql = "SELECT * FROM user WHERE id = ? ";

			// SELECTを実行し、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, id);
			ResultSet rs = pStmt.executeQuery();

			// 結果表に格納されたレコードの内容を

			if (!rs.next()) {
				return null;
			}

			int id1 = rs.getInt("id");
			String loginId = rs.getString("login_id");
			String name = rs.getString("name");
			Date birthDate = rs.getDate("birth_date");
			String password = rs.getString("password");
			String createDate = rs.getString("create_date");
			String updateDate = rs.getString("update_date");

			User user1 = new User(id1, loginId, name, birthDate, password, createDate, updateDate);
			return user1;

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}


	//更新メソッド
	public void UpDatePassword( String password1, String password2, String name, String birthdate,String Id) {
		Connection con = null;
		PreparedStatement stmt = null;



		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "UPDATE user SET  password= ?, name = ?,birth_date=?  WHERE id = ?";

			// SQLの?パラメータに値を設定
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, password1);
			pStmt.setString(2, name);
			pStmt.setString(3, birthdate);
			pStmt.setString(4, Id);

			code(password1);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}


	public void UpDate(String name, String birthdate,String Id) {
		Connection con = null;
		PreparedStatement stmt = null;

		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String sql = "UPDATE user SET  name =?,birth_date=?  WHERE id = ?";

			// SQLの?パラメータに値を設定
			PreparedStatement pStmt = con.prepareStatement(sql);
			pStmt.setString(1, name);
			pStmt.setString(2, birthdate);
			pStmt.setString(3, Id);

			int rs = pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}






	public String code(String password1) {

		String source = password1;
		//ハッシュアルゴリズム
		String algorithm = "MD5";

		//ハッシュ生成処理
		byte[] bytes=null;;
		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(StandardCharsets.UTF_8));
		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);
		//標準出力
		return result;
	}
}

/*

public List<User> findPickupByLoginId(String login_id){
	   Connection conn = null;
	   List<User> userList = new ArrayList<User>();

     try {
         // データベースへ接続
         conn = DBManager.getConnection();

         // TODO: 未実装：管理者以外を取得するようSQLを変更する
         // SELECT文を準備
          String sql ="SELECT login_id, name, birth_date FROM user WHERE birth_date BETWEEN ? AND ?";

          // SELECTを実行し、結果表を取得
         PreparedStatement pStmt = conn.prepareStatement(sql);
         pStmt.setString(1, birthstart);
         pStmt.setString(2, birthdayend);

         // SELECTを実行し、結果表を取得

         ResultSet rs = pStmt.executeQuery();

         // 結果表に格納されたレコードの内容を
         // Userインスタンスに設定し、ArrayListインスタンスに追加
         while (rs.next()) {
             String loginId = rs.getString("login_id");
             String name = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             User user = new User(loginId, name, birthDate);

             userList.add(user);
         }
     } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
 }


public List<User> findPickupByName(String name){
	   Connection conn = null;
	   List<User> userList = new ArrayList<User>();

     try {

         // データベースへ接続
         conn = DBManager.getConnection();

         // TODO: 未実装：管理者以外を取得するようSQLを変更する
         // SELECT文を準備
         String sql ="SELECT login_id, name, birth_date FROM user WHERE name LIKE %?%";

          // SELECTを実行し、結果表を取得
         PreparedStatement pStmt = conn.prepareStatement(sql);
         pStmt.setString(1, name);

         // SELECTを実行し、結果表を取得
         ResultSet rs = pStmt.executeQuery();

         // 結果表に格納されたレコードの内容を
         // Userインスタンスに設定し、ArrayListインスタンスに追加
         while (rs.next()) {
             String loginId = rs.getString("login_id");
             String name1 = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             User user = new User(loginId, name1, birthDate);

             userList.add(user);
         }
     } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
 }


public List<User> findPickupByBirth(String birthstart, String birthdayend){
	   Connection conn = null;
	   List<User> userList = new ArrayList<User>();

     try {

         // データベースへ接続
         conn = DBManager.getConnection();

         // TODO: 未実装：管理者以外を取得するようSQLを変更する
         // SELECT文を準備
         String sql ="SELECT login_id, name, birth_date FROM user WHERE birth_date BETWEEN ? AND ?";

          // SELECTを実行し、結果表を取得
         PreparedStatement pStmt = conn.prepareStatement(sql);
         pStmt.setString(1, birthstart);
         pStmt.setString(2, birthdayend);

         // SELECTを実行し、結果表を取得
         ResultSet rs = pStmt.executeQuery(sql);

         // 結果表に格納されたレコードの内容を
         // Userインスタンスに設定し、ArrayListインスタンスに追加
         while (rs.next()) {
             String loginId = rs.getString("login_id");
             String name1 = rs.getString("name");
             Date birthDate = rs.getDate("birth_date");
             User user = new User(loginId, name1, birthDate);

             userList.add(user);
         }
     } catch (SQLException e) {
         e.printStackTrace();
         return null;
     } finally {
         // データベース切断
         if (conn != null) {
             try {
                 conn.close();
             } catch (SQLException e) {
                 e.printStackTrace();
                 return null;
             }
         }
     }
     return userList;
 }




	public List<User> findPickupByLoginId(String login_id){
		   Connection conn = null;
		   List<User> userList = new ArrayList<User>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // TODO: 未実装：管理者以外を取得するようSQLを変更する
	            // SELECT文を準備
	            String sql ="SELECT login_id, name, birth_date FROM user WHERE login_id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
	            pStmt.setString(1, login_id);

	            // SELECTを実行し、結果表を取得

	            ResultSet rs = pStmt.executeQuery();

	            // 結果表に格納されたレコードの内容を
	            // Userインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	                String loginId = rs.getString("login_id");
	                String name = rs.getString("name");
	                Date birthDate = rs.getDate("birth_date");
	                User user = new User(loginId, name, birthDate);

	                userList.add(user);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            // データベース切断
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return userList;
	    }



			public List<User> findPickupByName(String name){
				   Connection conn = null;
				   List<User> userList = new ArrayList<User>();

			     try {
			         // データベースへ接続
			         conn = DBManager.getConnection();

			         // TODO: 未実装：管理者以外を取得するようSQLを変更する
			         // SELECT文を準備
			         String sql ="SELECT login_id, name, birth_date FROM user WHERE name LIKE %?%";

			         // SELECTを実行し、結果表を取得
			        PreparedStatement pStmt = conn.prepareStatement(sql);
			        pStmt.setString(1, name);

			         // SELECTを実行し、結果表を取得
			        ResultSet rs = pStmt.executeQuery();

			         // 結果表に格納されたレコードの内容を
			         // Userインスタンスに設定し、ArrayListインスタンスに追加
			         while (rs.next()) {
			             String loginId = rs.getString("login_id");
			             String name1 = rs.getString("name");
			             Date birthDate = rs.getDate("birth_date");
			             User user = new User(loginId, name1, birthDate);

			             userList.add(user);
			         }
			     } catch (SQLException e) {
			         e.printStackTrace();
			         return null;
			     } finally {
			         // データベース切断
			         if (conn != null) {
			             try {
			                 conn.close();
			             } catch (SQLException e) {
			                 e.printStackTrace();
			                 return null;
			             }
			         }
			     }
			     return userList;
			 }




			public List<User> findPickupByBirth(String birthstart,String birthdayend){
				   Connection conn = null;
				   List<User> userList = new ArrayList<User>();

			  try {
			      // データベースへ接続
			      conn = DBManager.getConnection();

			      // TODO: 未実装：管理者以外を取得するようSQLを変更する
			      // SELECT文を準備
			       String sql ="SELECT login_id, name, birth_date FROM user WHERE birth_date BETWEEN ? AND ?";

			       // SELECTを実行し、結果表を取得
			      PreparedStatement pStmt = conn.prepareStatement(sql);
			      pStmt.setString(1, birthstart);
			      pStmt.setString(2, birthdayend);

			      // SELECTを実行し、結果表を取得

			      ResultSet rs = pStmt.executeQuery();

			      // 結果表に格納されたレコードの内容を
			      // Userインスタンスに設定し、ArrayListインスタンスに追加
			      while (rs.next()) {
			          String loginId = rs.getString("login_id");
			          String name = rs.getString("name");
			          Date birthDate = rs.getDate("birth_date");
			          User user = new User(loginId, name, birthDate);

			          userList.add(user);
			      }
			  } catch (SQLException e) {
			      e.printStackTrace();
			      return null;
			  } finally {
			      // データベース切断
			      if (conn != null) {
			          try {
			              conn.close();
			          } catch (SQLException e) {
			              e.printStackTrace();
			              return null;
			          }
			      }
			  }
			  return userList;
}




   public List<User> findPickup(String loginId, String name, String birthstart, String birthdayend){
	   Connection conn = null;
	   List<User> userList = new ArrayList<User>();

        try {

            // データベースへ接続
            conn = DBManager.getConnection();

            // TODO: 未実装：管理者以外を取得するようSQLを変更する
            // SELECT文を準備
            String sql =" SELECT login_id, name, birth_date FROM user WHERE login_id = '?' and name='?' AND birth_date BETWEEN '?' AND '?'";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, name);
            pStmt.setString(3, birthstart);
            pStmt.setString(4, birthdayend);

            // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id1 = rs.getInt("id");
                String loginId1 = rs.getString("login_id");
                String name1 = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id1, loginId1, name1, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
























*/




